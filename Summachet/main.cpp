#include<iostream>

long long f(int x) {
	if (x == 0) {
		return 0;
	}
	if (x % 2 == 0) {
		return x + f(x - 1);
	}
	else {
		return f(x - 1);
	}
}

int main() {
	int N;
	N = 0;
	std::cout << "Enter N";
	std::cin >> N;
	f(N);
	std::cout << f(N);
	getchar();
	getchar();
}